# Используем официальный образ Ubuntu 24.04
FROM ubuntu:24.04

# Обновляем пакеты и устанавливаем openssh-server
RUN apt-get update && \
    apt-get install -y openssh-server sudo && \
    mkdir /var/run/sshd

# Настраиваем SSH: разрешаем вход по паролю и для root (для тестов)
RUN echo 'root:password' | chpasswd && \
    sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/' /etc/ssh/sshd_config

# Открываем порт 22 для SSH
EXPOSE 22

# Запускаем SSH-демон при старте контейнера
CMD ["/usr/sbin/sshd", "-D"]